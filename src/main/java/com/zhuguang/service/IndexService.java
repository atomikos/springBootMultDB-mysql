package com.zhuguang.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.zhuguang.App;
import com.zhuguang.db1.dao.DB1_UserMapper;
import com.zhuguang.db1.service.DB1_UserService;
import com.zhuguang.db2.dao.DB2_UserMapper;
import com.zhuguang.db2.service.DB2_UserService;
import com.zhuguang.entity.Users;

@Service
public class IndexService {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(IndexService.class);
	@Autowired
	private DB1_UserMapper db1UserMapper;
	@Autowired
	private DB2_UserMapper db2UserMapper;
	@Autowired
	private DB1_UserService db1UserService;
	@Autowired
	private DB2_UserService db2UserService;
	/**
	 *  atomikos效果：分布式事物。两个数据库都插入值
	 * @return
	 */
	@Transactional
	public void insertTwoDBs(String name, Integer age) {
		db1UserMapper.insert(name, age);
		db2UserMapper.insert(name, age);
	}

	@Transactional
	public void deleteAll() {
		db1UserMapper.deleteAll();
		//不同数据库。test1,test2
		//userService2.insertDB2(name, age);
		db2UserMapper.deleteAll();//test2   
//		int i = 1 / 0;//
	}

	/**
	 * atomikos效果：分布式事物。
	  *  演示发生异常分布式事物回滚
	  *  这里无论error 1、2、3，任何一处发生异常，分布式事物都会回滚
	  *  此方法效果等同于insertTwoDBsUseMapperWithError
	 */
	@Transactional //(rollbackFor = { Exception.class })
	public void insertTwoDBsWithError(String name, Integer age) {
		db1UserService.insert2DB1(name, age);
		db2UserService.insert2DB2(name, age);
		//int i = 1 / 0; // error 1
//		db2UserService.insertNotExistsTable(name, age);
	}
	/**
	 * atomikos效果：分布式事物。
	  *  演示发生异常分布式事物回滚
	  *  这里无论error 1、2、3，任何一处发生异常，分布式事物都会回滚
	  *  此方法效果等同于insertTwoDBsWithError
	 */
	@Transactional
	public void insertTwoDBsUseMapperWithError(String name, Integer age) {
		db1UserMapper.insert(name, age);
		db2UserMapper.insert(name, age);
//		db2UserMapper.insertNotExistsTable(name, age);
	}
	/**
	 * 如果去掉try catch，那么此方法等同于insertTwoDBsUseMapperWithError
	 * 事实证明一旦try catch，不显式抛出异常，那么分布式事物将失效。
	 * @param name
	 * @param age
	 */
	@Transactional
	public void insertTwoDBsWithTryCatch(String name, Integer age) {
		db1UserMapper.insert(name, age);
		db2UserMapper.insert(name, age);
		try {
			db2UserMapper.insertNotExistsTable(name, age);
		}catch (Exception e) {
			LOG.error("发生异常：" + e.getMessage());
		}
	}

	public List queryAll() {
		List all = new ArrayList();
		
		List<Users> list1 = db1UserService.queryAll();
		if(CollectionUtils.isEmpty(list1)) {
			all.add("db1 没有任何数据！");
		}else {
			all.addAll(list1);
		}
		
		List<Users> list2 = db2UserService.queryAll();
		if(CollectionUtils.isEmpty(list2)) {
			all.add("db2 没有任何数据！");
		}else {
			all.addAll(list2);
		}
		
		return all;
	}
	
}
